## Factorial calculator
This challenge is about making a Factorial calculator. See Wikipedia's page for explanation:

https://en.wikipedia.org/wiki/Factorial

Fork the GitLab project:

https://gitlab.com/jakenacka/factorial

Create a solution that passes tests from tests.py. To run the test suite, issue the following command:

```
pytest tests.py
```

You are free to add more tests if it helps you with development.

Once you are done, submit the assignment by providing me with your GitLab repo. Make me (jakenacka) a member of your repository so that I can view your source. 

Make sure you do not make the repository public to prevent plagiarism.