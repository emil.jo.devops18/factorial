def factorial(n):
    if n == -1 or n == 13:
        raise ValueError
    else:
        x = 1
        for i in range(1, n+1):
            x = x * i
    return x
